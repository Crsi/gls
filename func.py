#!/usr/bin/python3

DEBUG = False

def debug(*args, **kwargs):
	"""
	Print only if debugging is enabled
	"""
	
	if DEBUG:
		print(*args, **kwargs)

def f(x: float, y: float) -> float:
	"""
	Returns the x,y value to the function f
	"""
	
	return 0

def g(x: float, y: float) -> float:
	"""
	Returns the x,y value to the function g
	"""
	
	return x + 2*y

def generate2DArray(rangeA: range, rangeB: range, function) -> list:
	"""
	Generates and returns a 2D-array
	using the values produced by "function"
	"""
	
	result = []
	for i in rangeA:
		result.append([])
		for j in rangeB:
			result[i].append(function(i, j))
	return result

def localDefect(u: list, r: list, i: int, j: int, h: float) -> float:
	"""
	Returns the local defect for r[i][j] depending
	on the surrounding u-values
	"""
	
	return r[i][j] - (
		(
			u[i-1][j] + u[i+1][j] + u[i][j+1] + u[i][j-1]
			- 4 * u[i][j]
		) / h**2
	)
