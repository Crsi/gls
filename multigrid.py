#!/usr/bin/python3

import func
import grid

GS_STEPS_BEFORE = 2

class MultiGrid:
	"""
	Class representing a multi-grid system
	"""
	
	def __init__(self, n: int, accuracy: int, start: float = 0, end: float = 1):
		"""
		Plain constructor
		"""
		
		func.debug("Initializing MultiGrid ...")
		
		if n not in [2**i for i in range(1, 17)]:
			print("Var N is not a power of 2!")
			exit(2)
		
		# Store the number of intervals of the currently
		# used grid (and startN as a backup of it)
		self.n = n
		self.startN = n
		
		# Grid storage - n: Grid(n)
		self.grids = {}
		
		# Store the accuracy
		self.a = accuracy
		
		# Set the start and the end
		self.start = start
		self.end = end
	
	def main(self) -> int:
		"""
		Calls the routines to solve a multi-grid system
		"""
		
		func.debug("Starting MultiGrid main method ...")
		func.debug("Start using n={}".format(self.n))
		depth = 0
		steps = 0
		
		# Generate a new Grid using current n
		self.grids[self.n] = grid.Grid(self.n, self.a, self.start, self.end,
			True, useOmega = False)
		
		for x in range(10):
			func.debug("x={}".format(x))
			
			while self.n > 2:
				func.debug("Current depth:", depth)
				depth += 1
				
				# Execute as much GS steps as declared at the top of the file
				for a in range(GS_STEPS_BEFORE):
					self.grids[self.n].step()
					steps += 1
				
				self.n //= 2
				func.debug("MultiGrid (n={}): restricting ...".format(self.n))
				
				self.grids[self.n] = grid.Grid(self.n, self.a, self.start,
					self.end, useOmega = False)
				self.grids[self.n].r = self.grids[2 * self.n].restrict()
			
			# Interpolate every coarse grid to a better grid
			else:
				while self.n < self.startN:
					self.n *= 2
					func.debug("MultiGrid(n={}): interpolating ...".format(
						self.n))
					
					self.grids[self.n].u = self.grids[self.n/2].interpolate(
						self.grids[self.n].u)
					self.grids[self.n].step()
					steps += 1
					
			print("Current defect for n={} is {}".format(
				self.n, self.grids[self.n].defect()))
			
		func.debug("Finished main method of MultiGrid object {}. "
			"Produced grids: {}".format(self, self.grids))
		
		# Returns the number of GS steps used to produce the result
		return steps
