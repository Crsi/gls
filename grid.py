#!/usr/bin/python3

import math
import func
import _thread


class Grid:
	"""
	Class for representing one grid in a multi-grid cluster
	"""
	
	def __init__(self, n: int, accuracy: int,
		start: float = 0, end: float = 1,
		fixBorder: bool = False,
		useOmega: bool = True,
		useThreads: bool = True):
		"""
		Plain constructor
		"""
		
		# Number of intervals
		self.n = n
		
		# Number of points
		self.points = n + 1
		
		# Accuracy for the results of the GS system
		self.accuracy = accuracy
		
		# Step size
		self.h = (end - start) / n
		
		# Range for all points
		self.pointerRange = range(1, self.n)
		
		# Start values (stored in a list - each item is a start value)
		self.u = func.generate2DArray(range(0, self.points),
			range(0, self.points), lambda x, y: 0)
		
		# Fix the values of the borders if requested
		if fixBorder:
			for i in range(0, self.points):
				self.u[i][0] = func.g(i * self.h, 0)
				self.u[0][i] = func.g(0, i * self.h)
				self.u[self.n][i] = func.g(1, i * self.h)
				self.u[i][self.n] = func.g(i * self.h, 1)
			
		# Result values (stored in a list - each item is a result)
		self.r = func.generate2DArray(range(0, self.points),
			range(0, self.points), func.f)
		
		# Attribute omega as factor of strength
		if useOmega:
			self.omega = 2 / (1 + math.sin(math.pi * self.h))
		else:
			self.omega = 1
		
		# Start defect
		self.startDefect = self.defect()
		
		# Remember whether to use multi-threading
		self.useThreads = useThreads
	
	def defect(self) -> float:
		"""
		Calculates the defect of the current state of the GS system
		"""
		
		d = 0
		for i in self.pointerRange:
			for j in self.pointerRange:
				d = max(abs(func.localDefect(
					self.u, self.r, i, j, self.h)), d)
		return d
	
	def step(self):
		"""
		Calculates one step of the GS system
		"""
		
		def helper(i):
			"""
			Helper function for better threading-possibilities
			"""
			
			for j in self.pointerRange:
				self.u[i][j] += self.omega * (-0.25 *
					(
						self.h ** 2 * self.r[i][j]
						- self.u[i + 1][j] - self.u[i - 1][j]
						- self.u[i][j + 1] - self.u[i][j - 1]
					) - self.u[i][j])
		
		for i in self.pointerRange:
			if self.n >= 512 and self.useThreads:
				_thread.start_new_thread(helper, (i,))
			else:
				helper(i)
	
	def gs(self) -> int:
		"""
		Full iteration of all parts of the GS system
		"""
		
		steps = 0
		while self.defect() > self.startDefect / self.accuracy:
			self.step()
			steps += 1
		return steps
	
	def inject(self) -> list:
		"""
		Returns the simplest form of the result
		of the restricting operation (the injection)
		"""
		
		nc = self.n//2
		func.debug("Grid {}: nc={}".format(self, nc))
		rc = func.generate2DArray(range(0, nc), range(0, nc), lambda x, y: 0)
		func.debug("Grid {}: rc={}".format(self, rc))
		
		for ic in range(1, nc):
			i = 2 * ic
			for jc in range(1, nc):
				j = 2 * jc
				rc[ic][jc] = func.localDefect(self.u, self.r, i, j, self.h)
		
		return rc
		
	def restrict(self) -> list:
		"""
		Generates and returns a new coarse grid / point 2D-array
		"""
		
		return self.inject()
	
	def interpolate(self, fineU) -> list:
		"""
		Improves the given fine grid / point 2D-array
		to make its values much better
		"""
		
		for ic in range(self.n):
			i = 2 * ic
			for jc in range(self.n):
				j = 2 * jc
				
				fineU[i][j] += self.u[ic][jc]
				fineU[i+1][j] += 0.5 * (
					self.u[ic+1][jc] + self.u[ic][jc])
				fineU[i][j+1] += 0.5 * (
					self.u[ic][jc+1] + self.u[ic][jc])
				fineU[i+1][j+1] += 0.25 * (
					self.u[ic+1][jc+1] + self.u[ic][jc+1]
					+ self.u[ic+1][jc] + self.u[ic][jc])
		
		return fineU
		
	def start(self):
		"""
		Starts the main part to solve the GS
		"""
		
		steps = self.gs()
		#for i in self.u:
			#print(i)
		print("Steps:", steps)
