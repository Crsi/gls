#!/usr/bin/python3

import time
import func
import multigrid
import argparse

def initParsing() -> argparse.Namespace:
	"""
	Initializes the argparse arguments for the command-line interface
	"""
	
	parser = argparse.ArgumentParser(
		description = "Program to solve big one-dimensional GS systems")
	parser.add_argument(
		"number", type = int, help = "number of intervals")
	parser.add_argument(
		"accuracy", type = int, help = "accuracy to solve the GS system")
	parser.add_argument(
		"-s", "--start", type = int, help = "start of the border",
		default = 0)
	parser.add_argument(
		"-e", "--end", type = int, help = "end of the border",
		default = 1)
	parser.add_argument(
		"--timer", help = "use of a additional timing module",
		action = "store_true", default = False)
	parser.add_argument(
		"--debug", help = "enable debugging output",
		action = "store_true", default = False)
	return parser.parse_args()

if __name__ == "__main__":
	args = initParsing()
	print(args)
	args.accuracy = 10 ** args.accuracy
	func.DEBUG = args.debug
	
	if args.timer:
		start = time.clock()
		mg = multigrid.MultiGrid(args.number, args.accuracy, args.start, args.end)
		func.debug("Steps:", mg.main())
		end = time.clock()
		print(end - start)
	
	else:
		mg = multigrid.MultiGrid(args.number, args.accuracy, args.start, args.end)
		func.debug("Steps:", mg.main())
